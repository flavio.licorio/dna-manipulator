class DNAManipulator:
    def __init__(self, model):
        self.model = model
        self.RNA = []

    def invert(self, var):
        return var[::-1]

    def makeRNA(self, sequence, position=0):
        if position == len(sequence):
            return "".join(self.RNA)
        if sequence[position] in self.model:
            self.RNA.append(self.model[sequence[position]])
            return self.makeRNA(sequence, position+1)

    def searchMatch(self, sequence1, sequence2, missMatch: bool, position=0, matches=0):
        if position == len(sequence1):
            if missMatch:
                return {
                    "missmatches": len(sequence1)-matches,
                    "percentual": round((len(sequence1)-matches)/len(sequence1)*100, 2)
                }
            return {
                "matches": matches,
                "percentual": round(matches/len(sequence1)*100, 2)
            }
        if sequence1[position] == sequence2[position]:
            return self.searchMatch(sequence1, sequence2, missMatch, position+1, matches+1)
        return self.searchMatch(sequence1, sequence2, missMatch, position+1, matches)


RNAwithTemplateStrand = {
    "A": "U",
    "T": "A",
    "C": "G",
    "G": "C"
}
RNAwithCodingStrand = {
    "T": "U",
    "A": "A",
    "G": "G",
    "C": "C"
}

templateStrand = DNAManipulator(RNAwithTemplateStrand)
# print(templateStrand.makeRNA("TACTAGA"))

# codingStrand = DNAManipulator(RNAwithCodingStrand)
# print(codingStrand.makeRNA("ATGATCT"))

print(templateStrand.searchMatch("ACTGCTG", "AACGCTG", False))
print(templateStrand.searchMatch("ACTGCTG", "AACGCTG", True))
